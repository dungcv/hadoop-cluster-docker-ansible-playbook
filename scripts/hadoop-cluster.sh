#!/bin/bash

HADOOP_HOME=/opt/hadoop
start() {
  echo "Start ............"
  ${HADOOP_HOME}/sbin/start-journalnode.sh
  echo "wait 2 minute for journal ready"
  ${HADOOP_HOME}/sbin/start-dfs-without-journal.sh

  echo "Start Yarn"
  ${HADOOP_HOME}/sbin/start-yarn.sh

ssh root@master2 <<'ENDSSH'
  ${HADOOP_HOME}/sbin/yarn-daemon.sh start resourcemanager
ENDSSH

ssh root@journalnode <<'ENDSSH'
  ${HADOOP_HOME}/sbin/httpfs.sh start
  ${HADOOP_HOME}/sbin/mr-jobhistory-daemon.sh --config /opt/hadoop/etc/hadoop start historyserver
ENDSSH


  echo "Finish Start !"
}

stop() {
  echo "Stop ............"
  ${HADOOP_HOME}/sbin/stop-dfs.sh
  ${HADOOP_HOME}/sbin/stop-yarn.sh

ssh root@master2 <<'ENDSSH'
  ${HADOOP_HOME}/sbin/yarn-daemon.sh stop resourcemanager
ENDSSH

ssh root@journalnode <<'ENDSSH'
  ${HADOOP_HOME}/sbin/httpfs.sh stop
  ${HADOOP_HOME}/sbin/mr-jobhistory-daemon.sh --config /opt/hadoop/etc/hadoop stop historyserver
ENDSSH

  echo "Finish Stop !"
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/hadoop-cluster.sh [start|stop|restart]"
  ;;
esac