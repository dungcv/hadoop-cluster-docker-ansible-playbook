#!/bin/bash

start() {
docker run --name pgadmin4 \
           -h pgadmin4 \
           -p 5050:5050 \
           -v /redis01/pgadmin:/root/.pgadmin:rw \
           -d fenglc/pgadmin4
}

stop() {
docker stop pgadmin4
docker rm pgadmin4
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/pgadmin4.sh [start|stop|restart]"
  ;;
esac
