#!/bin/bash

# LINK docker: https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image

start() {
	docker run -d --name rstudio -h rstudio -p 8787:8787 -e USER=rstudio -e PASSWORD=rstudio15 -v /logs-data/share/docker-volume/rstudio:/home/ rocker/rstudio
}

stop() {
	docker stop rstudio
	docker rm rstudio
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/rstudio.sh [start|stop|restart]"
  ;;
esac