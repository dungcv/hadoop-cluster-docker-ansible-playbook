#!/bin/bash

HADOOP_HOME=/opt/hadoop
HADOOP_BIN=${HADOOP_HOME}/bin
PUT_COMMAND="${HADOOP_BIN}/hdfs dfs -put "
MKDIR_COMMAND="${HADOOP_BIN}/hdfs dfs -mkdir "

ROOT_PATH="/raw-logs/SOC/DNS_rawlog"
OUT_PATH="/data/dns/origin"

put_hour() {
	day=$1
	hour=$2
	hourBase10=10#$hour
	hourRound=$(( $(($hourBase10+1)) % 24 ))  
	hour2=$(printf %02d $hourRound)
	inPath="${ROOT_PATH}/${day}/dns.${hour}-[0-9][0-9]-[0-9][0-9]-${hour2}-[0-9][0-9]-[0-9][0-9].log.gz"
	outPath="${OUT_PATH}/${day}/${hour}"
	${MKDIR_COMMAND} "${outPath}"
	${PUT_COMMAND}  ${inPath} ${outPath}
}

day=$1
hour=$2
flag=$3

if [[ $flag == 'true' ]]; then
  ${MKDIR_COMMAND} "${OUT_PATH}/${day}"
fi

if [[ $hour == "all" ]]; then
	for i in {00..23};
	do
		put_hour $day $i
	done
else
	put_hour $day $hour
fi