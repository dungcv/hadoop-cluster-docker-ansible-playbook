#!/bin/bash

IN_PATH=$1
OUT_PATH=$2

for i in {00..23}
do
  j=0
  if [ $i == '08' ]; then
    j='09'
  elif [ $i == '09' ]; then
    j='10'
  else
    j=$(printf %02d $(($i+1)))
  fi

echo $j
cp ${IN_PATH}/dns.${i}\:00\:00-${j}\:00\:00.log.gz ${OUT_PATH}/dns-${i}-${j}-log.gz

done

echo "DONE."