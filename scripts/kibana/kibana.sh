#!/bin/bash

start() {
	docker run --name kibana -e ELASTICSEARCH_URL=http://172.27.11.156:9200 -p 5601:5601 -d kibana
}

stop() {
	docker stop kibana
	docker rm kibana
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/kibana.sh [start|stop|restart]"
  ;;
esac