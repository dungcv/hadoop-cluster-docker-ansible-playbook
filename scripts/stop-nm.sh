#!/bin/bash

for i in {71..90};
do
echo $i
ssh root@172.27.11.$i <<'ENDSSH'
  kill -9 $(jps | grep NodeManager | awk '{print $1}')
ENDSSH

done
