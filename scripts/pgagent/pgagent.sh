#!/bin/bash

# LINK: pgagent.sql: https://raw.githubusercontent.com/postgres/pgagent/master/sql/pgagent.sql
# Link: docker image: https://hub.docker.com/r/huggla/ubuntu-debootstrap-pgagent/

start() {
docker run --name pgagent \
           -h pgagent \
           -v "$(pwd)/startup.sh":/usr/bin/startup.sh \
           -d huggla/ubuntu-debootstrap-pgagent startup.sh
}

stop() {
docker stop pgagent
docker rm pgagent
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/pgagent.sh [start|stop|restart]"
  ;;
esac
