#!/bin/bash

namenode=${2:-172.27.11.61}
resourcemanager=${3:-172.27.11.61}

start() {
  /usr/bin/screen -dmS resourcemanager  ssh -L 0.0.0.0:50070:${namenode}:50070 dungvc2@${namenode}
  /usr/bin/screen -dmS namenode  ssh -L 0.0.0.0:8088:${resourcemanager}:8088 dungvc2@${resourcemanager}
  
  /usr/bin/screen -dmS slave01  ssh -L 0.0.0.0:24181:172.27.11.71:24181 dungvc2@172.27.11.71
  /usr/bin/screen -dmS slave02  ssh -L 0.0.0.0:24182:172.27.11.72:24182 dungvc2@172.27.11.72
  /usr/bin/screen -dmS slave03  ssh -L 0.0.0.0:24183:172.27.11.73:24183 dungvc2@172.27.11.73
  /usr/bin/screen -dmS slave04  ssh -L 0.0.0.0:24184:172.27.11.74:24184 dungvc2@172.27.11.74
  /usr/bin/screen -dmS slave05  ssh -L 0.0.0.0:24185:172.27.11.75:24185 dungvc2@172.27.11.75

  /usr/bin/screen -dmS slave06  ssh -L 0.0.0.0:24281:172.27.11.76:24281 dungvc2@172.27.11.76
  /usr/bin/screen -dmS slave07  ssh -L 0.0.0.0:24282:172.27.11.77:24282 dungvc2@172.27.11.77
  /usr/bin/screen -dmS slave08  ssh -L 0.0.0.0:24283:172.27.11.78:24283 dungvc2@172.27.11.78
  /usr/bin/screen -dmS slave09  ssh -L 0.0.0.0:24284:172.27.11.79:24284 dungvc2@172.27.11.79
  /usr/bin/screen -dmS slave10  ssh -L 0.0.0.0:24285:172.27.11.80:24285 dungvc2@172.27.11.80

  /usr/bin/screen -dmS slave11  ssh -L 0.0.0.0:24381:172.27.11.81:24381 dungvc2@172.27.11.81
  /usr/bin/screen -dmS slave12  ssh -L 0.0.0.0:24382:172.27.11.82:24382 dungvc2@172.27.11.82
  /usr/bin/screen -dmS slave13  ssh -L 0.0.0.0:24383:172.27.11.83:24383 dungvc2@172.27.11.83
  /usr/bin/screen -dmS slave14  ssh -L 0.0.0.0:24384:172.27.11.84:24384 dungvc2@172.27.11.84
  /usr/bin/screen -dmS slave15  ssh -L 0.0.0.0:24385:172.27.11.85:24385 dungvc2@172.27.11.85

  /usr/bin/screen -dmS slave16  ssh -L 0.0.0.0:24481:172.27.11.86:24481 dungvc2@172.27.11.86
  /usr/bin/screen -dmS slave17  ssh -L 0.0.0.0:24482:172.27.11.87:24482 dungvc2@172.27.11.87
  /usr/bin/screen -dmS slave18  ssh -L 0.0.0.0:24483:172.27.11.88:24483 dungvc2@172.27.11.88
  /usr/bin/screen -dmS slave19  ssh -L 0.0.0.0:24484:172.27.11.89:24484 dungvc2@172.27.11.89
  /usr/bin/screen -dmS slave20  ssh -L 0.0.0.0:24485:172.27.11.90:24485 dungvc2@172.27.11.90

  /usr/bin/screen -dmS hue ssh -L 0.0.0.0:8888:172.27.11.66:8888 dungvc2@172.27.11.66

  /usr/bin/screen -dmS ganglia ssh -L 0.0.0.0:8881:172.27.11.66:8881 dungvc2@172.27.11.66

  /usr/bin/screen -dmS jobhistory ssh -L 0.0.0.0:19888:172.27.11.65:19888 dungvc2@172.27.11.65
}

stop() {
    stopScreen namenode
    stopScreen resourcemanager

    stopScreen slave01
    stopScreen slave02
    stopScreen slave03
    stopScreen slave04
    stopScreen slave05
    stopScreen slave06
    stopScreen slave07
    stopScreen slave08
    stopScreen slave09
    stopScreen slave10
    stopScreen slave11
    stopScreen slave12
    stopScreen slave13
    stopScreen slave14
    stopScreen slave15
    stopScreen slave16
    stopScreen slave17
    stopScreen slave18
    stopScreen slave19
    stopScreen slave20

    stopScreen hue

    stopScreen ganglia

    stopScreen jobhistory
}

stopScreen() {
  screen_id=$(screen -ls | grep $1 | awk '{print $1}')
  arrs=(${screen_id//|/ })
  for i in "${arrs[@]}"
  do
   :
   /usr/bin/screen -X -S $i quit
  done
}

if [ $1 == "start" ]
then
  start
elif [ $1 == "stop" ]
then
  stop
else
  echo "start with [start\stop]"
fi
