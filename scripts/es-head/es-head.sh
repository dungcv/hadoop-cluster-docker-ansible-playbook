#!/bin/bash

start() {
	docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -v "$PWD/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml" elasticsearch
	docker run -d --name es-head --link elasticsearch:elasticsearch -p 9100:9100 mobz/elasticsearch-head:5
}

stop() {
	docker stop es-head
	docker rm es-head
	docker stop elasticsearch
	docker rm elasticsearch
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/es-head.sh [start|stop|restart]"
  ;;
esac