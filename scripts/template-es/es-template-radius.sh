#!/bin/bash

# Template for Elasticsearch version 5.0+
# _ttl is removed, using delete-by-query with crontab instead of.
curl -XPUT 172.27.11.156:9200/_template/template_radius -d '
{
  "template": "radius*",
  "order": 0,
  "settings": {
    "refresh_interval": "5s",
    "number_of_shards": 3,
    "number_of_replicas": 2
  },
  "mappings": {
      "connlog" : {
        "properties" : {
          "connect_type" : {
            "type": "keyword"
          },
          "content1" : {
            "type": "keyword"
          },
          "content2" : {
            "type": "keyword"
          },
          "name" : {
            "type": "keyword"
          },
          "session_id" : {
            "type" : "keyword"
          },
          "time": {
            "type": "date",
            "format": "date_time"
          }
        }
      }
    }
  }
}'


echo ""