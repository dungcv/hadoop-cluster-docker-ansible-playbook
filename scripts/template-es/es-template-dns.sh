#!/bin/bash

# Template for Elasticsearch version 5.0+
# _ttl is removed, using delete-by-query with crontab instead of.
curl -XPUT 172.27.11.156:9200/_template/template_dns -d '
{
  "template": "dns*",
  "order": 0,
  "settings": {
    "refresh_interval": "5s",
    "number_of_shards": 3,
    "number_of_replicas": 2
  },
  "mappings": {
      "docs" : {
        "properties" : {
          "timeStamp" : {
            "type": "date",
            "format": "date_time"
          },
          "uid" : {
            "type": "keyword"
          },
          "idOrigHost" : {
            "type": "keyword"
          },
          "idOrigPort" : {
            "type": "long"
          },
          "idRespHost" : {
            "type": "keyword"
          },
          "idRespPort" : {
            "type": "long"
          },
          "protocol" : {
            "type" : "keyword"
          },
          "transId" : {
            "type" : "keyword"
          },
          "rtt" : {
            "type" : "keyword"
          },
          "query" : {
            "type" : "keyword"
          },
          "qClass" : {
            "type" : "long"
          },
          "qClassName" : {
            "type" : "keyword"
          },
          "qType" : {
            "type" : "long"
          },
          "qTypeName" : {
            "type" : "keyword"
          },
          "rCode" : {
            "type" : "long"
          },
          "rCodeName" : {
            "type" : "keyword"
          },
          "aa" : {
            "type" : "boolean"
          },
          "tc" : {
            "type" : "boolean"
          },
          "rd" : {
            "type" : "boolean"
          },
          "ra" : {
            "type" : "boolean"
          },
          "z" : {
            "type" : "long"
          },
          "answers" : {
            "type" : "text"
          },
          "rejected" : {
            "type" : "boolean"
          }
        }
      }
    }
  }
}'


echo ""
