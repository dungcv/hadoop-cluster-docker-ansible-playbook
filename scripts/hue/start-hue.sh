#!/bin/bash

CURRENT_DIR=$(pwd)
SHARED_DIR=$CURRENT_DIR/shared
HOSTS_FILE=$SHARED_DIR/hosts
HUE_DIR=$SHARED_DIR/hue

IMAGE_HUE=hue-proxy 				  # Link: https://hub.docker.com/r/gethue/hue/

echo Starting hue...
docker run -d \
           -h hue \
           --name hue \
           -p 8888:8888 \
           -v $HOSTS_FILE:/tmp/hosts \
           -v $HUE_DIR/full-distributed-small.ini:/hue/desktop/conf/pseudo-distributed.ini \
           -v $HUE_DIR/startup.sh:/usr/local/bin/startup.sh \
           $IMAGE_HUE startup.sh

