#!/bin/bash

DAY=$1
HOUR=$2
HEADER="signin_distinct_count,signin_total_count,logoff_total_count,logoff_distinct_count,bras_id,time"

ROOT_PATH=/build/scripts/es-dump

INDEX="count_by_bras-${DAY}-${HOUR}"
PASSWORD='inf@db170120'

echo $PASSWORD

/bin/es2csv -i "${INDEX}" -u 172.27.11.156  -r -q '{"query": {"match_all": {}}}' -o "${ROOT_PATH}/${INDEX}.csv"

/bin/sed -i 1d "${ROOT_PATH}/${INDEX}.csv"

/bin/cat "${ROOT_PATH}/${INDEX}.csv" | PGPASSWORD=inf@db170120 /bin/psql -h 172.27.11.151 -p 5432 -U infdb -d inf_db -c \
	"COPY bras_count(${HEADER}) FROM stdin DELIMITER ',' CSV;"