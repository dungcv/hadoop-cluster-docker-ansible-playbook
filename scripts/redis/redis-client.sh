#!/bin/bash

HOST=$1
PORT=$2

docker run -it --rm redis redis-cli -h $HOST -p $PORT