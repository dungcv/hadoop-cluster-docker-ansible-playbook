#!/bin/bash

start() {
  PORT=$1
	docker run -d --name redis-server -p ${PORT}:6379 -v /redis01/redis-data/${PORT}:/data redis redis-server --appendonly yes
#	docker run --name redis -d redis redis-server --appendonly yes
#If persistence is enabled, data is stored in the VOLUME /data, which can be used with --volumes-from some-volume-container or -v /docker/host/dir:/data
}

stop () {
	docker stop redis-server
	docker rm redis-server
}

case "$1" in
  start)
    start $2
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/redis.sh [start|stop|restart]"
  ;;
esac