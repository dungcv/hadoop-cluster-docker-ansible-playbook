#!/bin/bash

start() {
echo "Start for master1"
/bin/systemctl start  gmond.service

echo "Start for master2"
ssh root@master2 <<'ENDSSH'
  /bin/systemctl start  gmond.service
ENDSSH

echo "Start for journalnode"
ssh root@journalnode <<'ENDSSH'
  /bin/systemctl start  gmond.service
ENDSSH

for i in {00..20}; do
echo "Start gmond for datanode$i"
ssh root@datanode$i <<'ENDSSH'
  /bin/systemctl start  gmond.service
ENDSSH
done
}

stop() {
echo "Stop for master1"
/bin/systemctl stop  gmond.service

echo "Stop for master2"
ssh root@master2 <<'ENDSSH'
  /bin/systemctl stop  gmond.service
ENDSSH

echo "Stop for journalnode"
ssh root@journalnode <<'ENDSSH'
  /bin/systemctl stop  gmond.service
ENDSSH

for i in {00..20}; do
echo "Stop gmond for datanode$i"
ssh root@datanode$i <<'ENDSSH'
  /bin/systemctl stop  gmond.service
ENDSSH
done
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/gmond-cluster.sh [start|stop|restart]"
  ;;
esac