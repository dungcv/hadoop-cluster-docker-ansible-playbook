# Reference from https://github.com/dwatrous/hadoop-multi-server-ansible

### Start 4 container docker

### Add ssh publish key of host to ~/.ssh/authorized_keys container

### Add known_hosts container docker to host

### Edit in hosts-dev, nodes-dev

### Start ansible playbook

```
ansible-playbook -i hosts-dev playbook.yml
```

### Reference
[hadoop-multi-server-ansible](https://github.com/dwatrous/hadoop-multi-server-ansible)
[ yarn.log-aggregation-enable](http://stackoverflow.com/questions/32713587/how-to-keep-yarns-log-files)

### Done
 

