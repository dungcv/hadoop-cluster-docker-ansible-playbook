#!/bin/bash

HADOOP_HOME=/opt/hadoop
start() {
  /opt/zookeeper/bin/zkServer.sh start

ssh root@master2 <<'ENDSSH'
  /opt/zookeeper/bin/zkServer.sh start
ENDSSH

ssh root@journalnode <<'ENDSSH'
  /opt/zookeeper/bin/zkServer.sh start
ENDSSH

  echo "Finish Stop !"
}

stop() {
  /opt/zookeeper/bin/zkServer.sh stop

ssh root@master2 <<'ENDSSH'
  /opt/zookeeper/bin/zkServer.sh stop
ENDSSH

ssh root@journalnode <<'ENDSSH'
  /opt/zookeeper/bin/zkServer.sh stop
ENDSSH

  echo "Finish Stop !"
}

case "$1" in
  start)
    start
  ;;
  stop)
    stop
  ;;
  restart)
    stop
    start
  ;;
  *)
    echo "$(pwd)/hadoop-cluster.sh [start|stop|restart]"
  ;;
esac