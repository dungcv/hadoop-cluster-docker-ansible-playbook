#!/bin/bash

CURRENT_DIR=$(pwd)
SHARE_DIR=${CURRENT_DIR}/shared/

docker run -d \
           --name ganglia \
           -h ganglia \
           -v ${SHARE_DIR}/gmetad.conf:/etc/ganglia/gmetad.conf \
           -v ${SHARE_DIR}/gmond.conf:/etc/ganglia/gmond.conf \
           -v ${SHARE_DIR}/data:/var/lib/ganglia \
           -p 0.0.0.0:8881:80  \
           -p 0.0.0.0:8649:8649 \
           wookietreiber/ganglia